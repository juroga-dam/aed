/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package copiarFichero;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Scanner;

/**
 *
 * @author juan
 */
public class CopiarFichero {
    
    public void listar(String ruta){
           
        try {
            File fichero=new File(ruta);
            File[] listadeArchivos = fichero.listFiles();
            if(!fichero.isDirectory()){
                System.out.println("Ruta indicada no válida");
                System.exit(0);
            }
            if (listadeArchivos == null || listadeArchivos.length == 0){
                System.out.println("No se encuentran elementos en la carpeta indicada");
                System.exit(0);
            }
            else {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                for (int i=0; i< listadeArchivos.length; i++) {
                    File archivo = listadeArchivos[i];
                    System.out.println(String.format("%s (%s) - %d - %s",
                            archivo.getName(),
                            archivo.isDirectory() ? "Carpeta" : "Archivo",
                            archivo.length(),
                            sdf.format(archivo.lastModified())
                ));
                }
            }
        }
        catch (Exception ex) {
        System.out.println("Error al buscar en la ruta indicada"); }
    }
    
    public static void main (String[] args){
       
        CopiarFichero copia = new CopiarFichero();
       
        //Solicitar el ruta a buscar
        System.out.print("Introduce ruta a buscar: ");
        Scanner scanner = new Scanner(System.in);
        String rutaABuscar = scanner.nextLine();
        
        copia.listar(rutaABuscar);
        try {
        //Solicitar el nombre del fichero a copiar
        System.out.print("Introduce del fichero a copiar: ");
        //Scanner scanner = new Scanner(System.in);
        String ficheroACopiar = scanner.nextLine();
        //Separar el nombre del fichero y la extensión para añadir la palabra copia antes de la extension
        String[] partesFichero = ficheroACopiar.split("\\.");
        String ficheroFinal =  partesFichero[0]+"Copiado."+partesFichero[1];
                 
        
            Path rutaOrigen = Paths.get(rutaABuscar.concat(ficheroACopiar));        
            Path rutaDestino = Paths.get(rutaABuscar.concat(ficheroFinal));
            Files.copy(rutaOrigen, rutaDestino, StandardCopyOption.REPLACE_EXISTING);
            } catch (FileAlreadyExistsException e) {
                System.out.println("el destino existe");
        } catch (IOException ex) {
            System.out.println("Error de fichero");
        }
        
        copia.listar(rutaABuscar);
    }
}
