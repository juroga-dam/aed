/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package buscarTexto;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author juan
 */
public class BuscarTexto {
 
    public static void main (String[] args) {
        
        String textoABuscar = "Hola, este es un archivo de texto de ejemplo";
        String linea;
        int numLinea = 1;
        boolean encontrado = false;
        
        File f = new File("/home/juan/datos/DAM/NetBeansProjects/aed/UT2/BuscarTexto/src/buscarTexto/texto.txt");
        
        // Comprobamos que exista el fichero
        if (f.exists()){
            // Comprobamos que el fichero tenga permiso de lectura
            if (f.canRead()){
                try {
                    Scanner entrada = new Scanner(f);
                    while (entrada.hasNext()) { //mientras no se llegue al final del fichero
                        linea = entrada.nextLine();  
                        if (linea.contains(textoABuscar)) {   //Si se encuentra se muestra mensaje por pantalla.         
                            System.out.println("Linea " + numLinea + ": " + linea);
                            encontrado = true;
                        }
                        numLinea++; //se incrementa el contador de líneas
                    }
                    if (!encontrado){
                       System.out.println("Texto no encontrado"); 
                    }
                } catch (FileNotFoundException ex) {
                    System.out.println("Fichero no encontrado");
                }
            }
            else {
                System.out.println("El fichero no tiene permiso de lectura");
            }
        }
        else {
            System.out.print("EL fichero de datos no existe");
        }
    }
}
