/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package capitalizarTexto;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author juan
 */
public class CapitalizarTexto {

    public static String capitalizar(String cadena) {

        String[] cadenaEspacios = cadena.split(" ");
        StringBuilder cadenaNueva = new StringBuilder();
        for (int i = 0; i < cadenaEspacios.length; i++) {
            String palabra = cadenaEspacios[i];
            char primeraLetra = palabra.charAt(0); //extrae la primera letar y la capitaliza
            cadenaNueva.append(Character.toUpperCase(primeraLetra)); //añade la primera letra en mayusculas a la cadena
            cadenaNueva.append(palabra.substring(1)); // añade el resto de la palabra a la cadena
            if (i < cadenaEspacios.length - 1) { // sde añade un espacio tras cada palabra, menos al final de la ultima
                cadenaNueva.append(" ");
            }
        }

        return cadenaNueva.toString();
    }

    public static void main(String[] args) {
        
        String fileOrigen = "/home/juan/datos/DAM/NetBeansProjects/aed/UT2/AED-UT2/src/capitalizarTexto/texto.txt";
        String fileDestino = "/home/juan/datos/DAM/NetBeansProjects/aed/UT2/AED-UT2/src/capitalizarTexto/textoCapìtalizado.txt";
      
        File ficheroOrigen = new File(fileOrigen);
                
        if (ficheroOrigen.exists()){ // Comprobamos que exista el fichero
            if (ficheroOrigen.canRead()){ // Comprobamos que el fichero tenga permiso de lectura
                try {
                    Scanner entrada = new Scanner(ficheroOrigen);
                    FileWriter ficheroDestino = new FileWriter(fileDestino);
                    //ficheroDestino.createNewFile();
                    while (entrada.hasNext()) { //mientras no se llegue al final del fichero
                        ficheroDestino.write(CapitalizarTexto.capitalizar(entrada.nextLine()) + "\n"); // Escribe en el fichero destino cada línea capitalizada por la funcion
                    }
                    ficheroDestino.close();
                    
                } catch (FileNotFoundException ex) {
                    System.out.println("Error de fichero, no se encuantra");
                } catch (StringIndexOutOfBoundsException e){
                    System.out.println("No se pudo continuar tratando texto. Error de fichero");
                } catch (IOException ex) {
                    System.out.println("Error al crear el fichero destino");
                }
            }
            else {
                System.out.println("El fichero origen no tiene permiso de lectura");
            }
        }
        else {
            System.out.print("El fichero origen no existe");
        }
    }
}
