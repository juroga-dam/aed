/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package datosPersonales;

/**
 *
 * @author juan
 */
public class Persona {
    
    private String nombre;
    private String apellidos;
    private String edad;
    private String infoAdicional;

    public Persona(){}
    
    public Persona(String nombre, String apellidos, String edad, String info){
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.edad = edad;
        this.infoAdicional = info;
    }    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getInfoAdicional() {
        return infoAdicional;
    }

    public void setInfoAdicional(String infoAdicional) {
        this.infoAdicional = infoAdicional;
    }
    
    public String[] toArrayString(){
        String[] s = new String[4];
        s[0] = nombre;
        s[1] = apellidos;
        s[2] = edad;
        s[3] = infoAdicional;
        return s;
    }

}
