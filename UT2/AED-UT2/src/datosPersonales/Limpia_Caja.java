/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package datosPersonales;

import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JTextArea;

/**
 *
 * @author juan
 */
public class Limpia_Caja {
    public void limpiar_texto(JPanel panel) {
    for (int i = 0; panel.getComponents().length > i; i++) {
        if (panel.getComponents()[i] instanceof JTextField) {
            ((JTextField) panel.getComponents()[i]).setText("");
        } 
        if (panel.getComponents()[i] instanceof JTextArea) {
            ((JTextArea) panel.getComponents()[i]).setText("");
        }
        /*if (panel.getComponents()[i] instanceof JTable) {
            ((JTable) panel.getComponents()[i]).setText("");
        }*/
    }
    }
}
