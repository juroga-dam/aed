package org.example;

import java.sql.Date;

public class EmpClass {
    private short empno;
    private String ename;
    private String job;
    private Short mgr;
    private String hiredate;
    private Integer sal;
    private Integer comm;
    private int deptno;

    public EmpClass(String nombre, String puesto, Short manager, String fecha, int salario, int comision, int numDepto, short numEmp) {
        this.ename = nombre;
        this.job = puesto;
        this.mgr = manager;
        this.hiredate = fecha;
        this.sal = salario;
        this.comm = comision;
        this.deptno = numDepto;
        this.empno = numEmp;

    }

    public short getEmpno() {
        return empno;
    }

    public void setEmpno(short empno) {
        this.empno = empno;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public Short getMgr() {
        return mgr;
    }

    public void setMgr(Short mgr) {
        this.mgr = mgr;
    }

    public String getHiredate() {
        return hiredate;
    }

    public void setHiredate(String hiredate) {
        this.hiredate = hiredate;
    }

    public Integer getSal() {
        return sal;
    }

    public void setSal(Integer sal) {
        this.sal = sal;
    }

    public Integer getComm() {
        return comm;
    }

    public void setComm(Integer comm) {
        this.comm = comm;
    }

    public int getDeptno() {
        return deptno;
    }

    public void setDeptno(byte deptno) {
        this.deptno = deptno;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EmpClass empClass = (EmpClass) o;

        if (empno != empClass.empno) return false;
        if (deptno != empClass.deptno) return false;
        if (ename != null ? !ename.equals(empClass.ename) : empClass.ename != null) return false;
        if (job != null ? !job.equals(empClass.job) : empClass.job != null) return false;
        if (mgr != null ? !mgr.equals(empClass.mgr) : empClass.mgr != null) return false;
        if (hiredate != null ? !hiredate.equals(empClass.hiredate) : empClass.hiredate != null) return false;
        if (sal != null ? !sal.equals(empClass.sal) : empClass.sal != null) return false;
        if (comm != null ? !comm.equals(empClass.comm) : empClass.comm != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) empno;
        result = 31 * result + (ename != null ? ename.hashCode() : 0);
        result = 31 * result + (job != null ? job.hashCode() : 0);
        result = 31 * result + (mgr != null ? mgr.hashCode() : 0);
        result = 31 * result + (hiredate != null ? hiredate.hashCode() : 0);
        result = 31 * result + (sal != null ? sal.hashCode() : 0);
        result = 31 * result + (comm != null ? comm.hashCode() : 0);
        result = 31 * result + (int) deptno;
        return result;
    }
}
