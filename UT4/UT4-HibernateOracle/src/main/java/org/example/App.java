package org.example;


import com.fasterxml.classmate.AnnotationConfiguration;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;


//import javax.security.auth.login.Configuration;

public class App
{
    public static void altaEmpleado(EmpClass empleado) {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.save(empleado);
        tx.commit();
        session.close();
    }
    public static String fecha(){
        LocalDateTime fecha = LocalDateTime.now();
        DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd-MM-yy");
        return fecha.format(formato);
    }
    public static void bajaEmpleado(EmpClass empleado) {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.delete(empleado);
        tx.commit();
        session.close();
    }

    public static <T> List<T> consulta(String ConsultaSQL) {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        Query query = session.createQuery(ConsultaSQL);
        return query.list();
    }

    public static void main( String[] args )
    {
        /*short numEmp = 0007;
        short numDepto = 40;
        String nombre = "JOSE";
        String puesto = "SALESMAN";
        Short manager = 7698;
        String Fecha = fecha();
        int salario = 2300, comision = 250;
        EmpClass newEmpleado = new EmpClass(nombre, puesto, manager, Fecha, salario, comision, numDepto, numEmp);
        //altaEmpleado(newEmpleado);
        bajaEmpleado(newEmpleado); */

        //Listar los empleados
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        String hql = "FROM EmpClass as empleado INNER JOIN empleado.deptno as departamento";
        List<?> list = session.createQuery(hql).list();
        for(int i=0; i<list.size(); i++) {
            Object[] row = (Object[]) list.get(i);
            EmpClass empleado = (EmpClass) row[0];
            DeptClass departamento = (DeptClass) row[1];
            System.out.println("Numero empleado: "+empleado.getEmpno()+", Nombre: "+ empleado.getEname() +
                    ", Salario: "+ empleado.getSal() +", Departamento: "+ departamento.getDname() + ", Localizacion: " + departamento.getLoc());
        }
        session.close();
    }
}
