package org.example;

public class DeptClass {
    private byte deptno;
    private String dname;
    private String loc;

    public byte getDeptno() {
        return deptno;
    }

    public void setDeptno(byte deptno) {
        this.deptno = deptno;
    }

    public String getDname() {
        return dname;
    }

    public void setDname(String dname) {
        this.dname = dname;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeptClass deptClass = (DeptClass) o;

        if (deptno != deptClass.deptno) return false;
        if (dname != null ? !dname.equals(deptClass.dname) : deptClass.dname != null) return false;
        if (loc != null ? !loc.equals(deptClass.loc) : deptClass.loc != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) deptno;
        result = 31 * result + (dname != null ? dname.hashCode() : 0);
        result = 31 * result + (loc != null ? loc.hashCode() : 0);
        return result;
    }
}
