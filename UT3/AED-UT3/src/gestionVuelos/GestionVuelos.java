/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionVuelos;

import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Juan
 */
public class GestionVuelos {
    
    private static List<Vuelo> listarVuelos() {

	List<Vuelo> vuelos = new ArrayList<>();
	try {
            BaseDatos instance = BaseDatos.getInstance();
            ResultSet resultSet = instance.consultaBD("select * from vuelos");
            
            while (resultSet.next()) {
		Vuelo vuelo = new Vuelo();
		vuelo.setCodigoVuelo(resultSet.getString("COD_VUELO"));
		vuelo.setDestino(resultSet.getString("DESTINO"));
		vuelo.setHoraSalida(resultSet.getString("HORA_SALIDA"));
		vuelo.setProcedencia(resultSet.getString("PROCEDENCIA"));
		vuelos.add(vuelo);
            }
            instance.cerrarConsulta();
        } catch (SQLException e) {
            e.printStackTrace();
	}
            return vuelos;
	}
    
    private static List<Vuelo> listarVuelos(String cod_vuelo) {

	List<Vuelo> vuelos = new ArrayList<>();
	try {
            BaseDatos instance = BaseDatos.getInstance();
            ResultSet resultSet = instance.consultaBD(String.format("select * from vuelos where COD_VUELO = ('%s')", cod_vuelo.toUpperCase()));
            
            while (resultSet.next()) {
		Vuelo vuelo = new Vuelo();
		vuelo.setCodigoVuelo(resultSet.getString("COD_VUELO"));
		vuelo.setDestino(resultSet.getString("DESTINO"));
		vuelo.setHoraSalida(resultSet.getString("HORA_SALIDA"));
		vuelo.setProcedencia(resultSet.getString("PROCEDENCIA"));
		vuelos.add(vuelo);
            }
            instance.cerrarConsulta();
        } catch (SQLException e) {
            e.printStackTrace();
	}
            return vuelos;
	}
    
    
    private static List<Pasajero> listarPasajeros() {

	List<Pasajero> pasajeros = new ArrayList<>();
	try {
            BaseDatos instance = BaseDatos.getInstance();
            ResultSet resultSet = instance.consultaBD("select * from pasajeros");
            
            while (resultSet.next()) {
		Pasajero pasajero = new Pasajero();
		pasajero.setNumero(resultSet.getInt("NUM"));
                pasajero.setCodigoVuelo(resultSet.getString("COD_VUELO"));
		pasajero.setTipoPlaza(resultSet.getString("TIPO_PLAZA"));
		pasajero.setFumador(resultSet.getString("FUMADOR"));
		pasajeros.add(pasajero);
            }
            instance.cerrarConsulta();
        } catch (SQLException e) {
            e.printStackTrace();
	}
            return pasajeros;
	}
    
    private static List<Pasajero> listarPasajeros(String cod_vuelo) {

	List<Pasajero> pasajeros = new ArrayList<>();
	try {
            BaseDatos instance = BaseDatos.getInstance();
            ResultSet resultSet = instance.consultaBD(String.format("select * from pasajeros where COD_VUELO = ('%s')", cod_vuelo.toUpperCase()));
            
            while (resultSet.next()) {
		Pasajero pasajero = new Pasajero();
		pasajero.setNumero(resultSet.getInt("NUM"));
                pasajero.setCodigoVuelo(resultSet.getString("COD_VUELO"));
		pasajero.setTipoPlaza(resultSet.getString("TIPO_PLAZA"));
		pasajero.setFumador(resultSet.getString("FUMADOR"));
		pasajeros.add(pasajero);
            }
            instance.cerrarConsulta();
        } catch (SQLException e) {
            e.printStackTrace();
	}
            return pasajeros;
	}
  
    private static boolean comprobarVuelo(String cod_vuelo){
        List<Vuelo> vuelos = listarVuelos(cod_vuelo);
        boolean encontrado = false;
        if (vuelos.size() > 0){
            encontrado = true;
        }
        return encontrado;
    }
    
    private static void crearVuelo(String cod_vuelo) throws UnsupportedEncodingException{
        PrintStream out = new PrintStream(System.out, true, "UTF-8");
        out.println("Introduzca los datos del nuevo vuelo:");
        String horaSalida, destino, procedencia;
        int plazasFumador, plazasNoFumador, plazasTurista, plazasPrimera;
        Scanner scanner = new Scanner(System.in);
        
        out.print("Introduzca la hora de salida (formato dd/mm/yy-HH:MM): ");
        horaSalida = scanner.next();
        
        out.print("Introduzca procedencia del vuelo: ");
        procedencia = scanner.next();
        
        out.print("Introduzca destino del vuelo: ");
        destino = scanner.next();
  
        out.print("Introduzca número de plazas de acomodación turista: ");
        plazasTurista = scanner.nextInt();

        out.print("Introduzca número de plazas de acomodación primera clase: ");
        plazasPrimera = scanner.nextInt();
        
        out.print("Introduzca número de plazas fumadores: ");
        plazasFumador = scanner.nextInt();

        out.print("Introduzca número de plazas NO fumadores: ");
        plazasNoFumador = scanner.nextInt();
        out.println("");
        
        Vuelo vuelo = new Vuelo();
	vuelo.setCodigoVuelo(cod_vuelo.toUpperCase());
	vuelo.setHoraSalida(horaSalida);
	vuelo.setProcedencia(procedencia.toUpperCase());
	vuelo.setDestino(destino.toUpperCase());
	vuelo.setPlazasFumador(plazasFumador);
	vuelo.setPlazasNoFumador(plazasNoFumador);
	vuelo.setPlazasTurista(plazasTurista);
	vuelo.setPlazasPrimera(plazasPrimera);
        
        try {
            BaseDatos instance = BaseDatos.getInstance();
            instance.consultaBD(String.format("insert into vuelos values ('%s','%s','%s','%s','%d','%d','%d','%d')",
											  vuelo.getCodigoVuelo(),
                                                                                          vuelo.getHoraSalida(),
											  vuelo.getDestino(),
                                                                                          vuelo.getProcedencia(),
											  vuelo.getPlazasFumador(),
                                                                                          vuelo.getPlazasNoFumador(),
											  vuelo.getPlazasTurista(),
                                                                                          vuelo.getPlazasPrimera()));
            instance.cerrarConsulta();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Ha ocurrido un error al introducir el vuelo");
	}
        
    }
    
    private static void borrarVuelo(String cod_Vuelo) throws SQLException {
        BaseDatos instance = BaseDatos.getInstance();
        instance.consultaBD(String.format("delete from vuelos where COD_VUELO = '%s'",cod_Vuelo));
        instance.cerrarConsulta();
    }
    
    private static void actualizarVuelo(String cod_Vuelo) throws SQLException {
        BaseDatos instance = BaseDatos.getInstance();
        instance.consultaBD(String.format("update VUELOS set PLAZAS_NO_FUMADOR = PLAZAS_NO_FUMADOR + PLAZAS_FUMADOR , PLAZAS_FUMADOR = 0 where COD_VUELO = '%s'",cod_Vuelo));
        instance.cerrarConsulta();
    }
        
    public static void main (String[] args) throws UnsupportedEncodingException, SQLException {
        PrintStream out = new PrintStream(System.out, true, "UTF-8");
         out.print("Gestión de vuelos\n---------------------------------------\n1.- Listar información general de vuelos\n2.- Mostrar información de los pasajeros\n3.- Ver información de pasajeros de un vuelo\n4.- Añadir un vuelo\n5.- Borrar un vuelo\n6.- Modificar un vuelo (Fumadores/No fumadores)\nPara salir introduzca (s/S)\nIntroduzca una opción: ");
            Scanner scanner = new Scanner(System.in);
            String opcion;
            
         do {
             opcion = scanner.next(); 
             if (opcion.equals("1")){
                out.println("Opción 1 - Listado de vuelos");
                List<Vuelo> vuelos = listarVuelos();
		for (Vuelo vuelo : vuelos) {
		out.println(String.format("----------------------\nFecha: %s \nCodigo: %s \nProcedencia: %s \nDestino: %s", 
                                                                                                     vuelo.getHoraSalida(),
                                                                                                     vuelo.getCodigoVuelo(),
                                                                                                     vuelo.getProcedencia(),
                                                                                                     vuelo.getDestino()));
                }
             } else if (opcion.equals("2")){
                out.println("Opción 2 - Listado de pasajeros");
                List<Pasajero> pasajeros = listarPasajeros();
		for (Pasajero pasajero : pasajeros) {
		out.println(String.format("----------------------\nNúmero: %d \nCodigo de vuelo: %s \nTipo de plaza: %s \nFumador: %s", 
                                                                                                     pasajero.getNumero(),
                                                                                                     pasajero.getCodigoVuelo(),
                                                                                                     pasajero.getTipoPlaza(),
                                                                                                     pasajero.getFumador()));
                }
             } else if (opcion.equals("3")){
                out.println("Opción 3 - Ver información de pasajeros de un vuelo");
                out.print("\nIndique el número de vuelo: ");
                scanner = new Scanner(System.in);
                String vueloBuscado = scanner.next();
                
                List<Pasajero> pasajeros = listarPasajeros(vueloBuscado);
		for (Pasajero pasajero : pasajeros) {
		out.println(String.format("----------------------\nNúmero: %d \nCodigo de vuelo: %s \nTipo de plaza: %s \nFumador: %s", 
                                                                                                     pasajero.getNumero(),
                                                                                                     pasajero.getCodigoVuelo(),
                                                                                                     pasajero.getTipoPlaza(),
                                                                                                     pasajero.getFumador()));
                }
             } else if (opcion.equals("4")){
                 out.println("Opción 4 - Añadir un vuelo");
                 out.print("\nIndique el número de vuelo a crear: ");
                 scanner = new Scanner(System.in);
                 String numVueloNuevo = scanner.next();
                 
                 if (comprobarVuelo(numVueloNuevo)){
                     out.println("El número de vuelo indicado ya existe");
                 } else {
                     crearVuelo(numVueloNuevo);
                     out.println("Se ha creado el nuevo vuelo");
                 }
                 
             } else if (opcion.equals("5")){
                 out.println("Opción 5 - Borrar un vuelo");
                 out.print("\nIndique el número de vuelo a borrar: ");
                 scanner = new Scanner(System.in);
                 String numVueloBorrar = scanner.next();
                 
                 if (!comprobarVuelo(numVueloBorrar)){
                     out.println("El número de vuelo indicado no existe");
                 } else {
                     out.print(String.format("\nPara confirmar que se va a eliminar el vuelo %s, debe volver a introducir el número de vuelo: ",numVueloBorrar));
                     scanner = new Scanner(System.in);
                     String numVueloConfirmado = scanner.next();
                     if (numVueloConfirmado.equals(numVueloBorrar)){
                         borrarVuelo(numVueloConfirmado);
                         out.println(String.format("Se ha borrado el vuelo %s", numVueloConfirmado));
                     } else {
                         out.print("El número de vuelo no coincide");
                     }
                 }
                 
             } else if (opcion.equals("6")){
                 out.println("Opción 6 - Cambiar vuelo de fumadores a no fumadores");
                 out.print("\nIndique el número de vuelo a cambiar: ");
                 scanner = new Scanner(System.in);
                 String numVueloUpdate = scanner.next();
                 
                 if (!comprobarVuelo(numVueloUpdate)){
                     out.println("El número de vuelo indicado no existe");
                 } else {
                     out.print(String.format("\nPara confirmar el cambio a NO fumadores del vuelo %s, debe volver a introducir el número de vuelo: ",numVueloUpdate));
                     scanner = new Scanner(System.in);
                     String numVueloConfirmado = scanner.next();
                     if (numVueloConfirmado.equals(numVueloUpdate)){
                         actualizarVuelo(numVueloConfirmado);
                         out.println(String.format("Se ha actualizado el vuelo %s", numVueloConfirmado));
                     } else {
                         out.print("El número de vuelo no coincide");
                     }
                 }
                                 
             } else if (opcion.toLowerCase().equals("s")){
                 out.println("Fin del programa");
             } else if (opcion.toLowerCase().equals("m")) {
                 out.println("Gestión de vuelos\n---------------------------------------\n1.- Listar información general de vuelos\n2.- Mostrar información de los pasajeros\n3.- Ver información de pasajeros de un vuelo\n4.- Añadir un vuelo\n5.- Borrar un vuelo\n6.- Modificar un vuelo (Fumadores/No fumadores)\nPara salir introduzca (s/S)");
             } else {
                 out.println("Opción incorrecta");
             }
             out.print("\nVer menú introduzca (m/M) \nIntroduzca una opción: ");           
         } while ((opcion.compareToIgnoreCase("s"))!=0);
     }
    
}
