/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionVuelos;

//import com.sun.jdi.connect.spi.Connection;
//import java.beans.Statement;
/*import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;*/
import java.sql.*;



/**
 *
 * @author Juan
 */
public class BaseDatos {
    
    private static BaseDatos instancia = null;
    private static boolean permitirInstancianueva;
    private String urljdbc = "jdbc:oracle:thin:@localhost:1521:FREE";
    private String usuario = "system";
    private String password = "DAMad23";
    private Connection conexion;
    private Statement smt;
    
    
    BaseDatos() throws Exception {
        if (!permitirInstancianueva)
            throw new Exception("No se puede crear la instancia, usa getInstance");
    }

    public static BaseDatos getInstance() {
        if (instancia == null) {
            permitirInstancianueva = true;
            try {
                instancia = new BaseDatos();
            } catch (Exception e) {
                e.printStackTrace();
            }
            permitirInstancianueva = false;
        }
        return instancia;
    }
    
    public ResultSet consultaBD(String consulta) throws SQLException{
        DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
        conexion = DriverManager.getConnection(urljdbc, usuario, password);
        smt = conexion.createStatement();     
        ResultSet rset = smt.executeQuery(consulta);
        return rset;
    }

    public void cerrarConsulta() throws SQLException {
        conexion.close();
    }
        
        
    public static BaseDatos getInstancia() {
        return instancia;
    }

    public static void setInstancia(BaseDatos instancia) {
        BaseDatos.instancia = instancia;
    }

    public static boolean isPermitirInstancianueva() {
        return permitirInstancianueva;
    }

    public static void setPermitirInstancianueva(boolean permitirInstancianueva) {
        BaseDatos.permitirInstancianueva = permitirInstancianueva;
    }

    public String getUrljdbc() {
        return urljdbc;
    }

    public void setUrljdbc(String urljdbc) {
        this.urljdbc = urljdbc;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Connection getConn() {
        return conexion;
    }

    public void setConn(Connection conn) {
        this.conexion = conn;
    }

    public Statement getStmt() {
        return smt;
    }

    public void setStmt(Statement stmt) {
        this.smt = stmt;
    }
    
    
    
}
