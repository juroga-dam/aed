package JefeHijo;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Query;
import java.io.File;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

public class Actions{
    
    private static ObjectContainer baseDatos;
    private static Actions action = new Actions();
    
    public static void crearBD(){
        File fichero=new File("BDJefeHijo.db4o");
        if (fichero.exists()){
            System.out.println("Se ha encontrado el archivo baseDatos.db4o. Se procede a su borrado.");
            fichero.delete();
        }

        /*Este código anterior lo ponemos por si la base de datos ya existiera y quisiéramos empezar desde el principio.*/
        baseDatos= Db4oEmbedded.openFile("BDJefeHijo.db4o");
        baseDatos.store(new Jefe("Ángel", 5, 53,new Hijo("Gustavo", 7)));
        baseDatos.store(new Jefe("Nieves", 3, 45,new Hijo("Iván", 3)));
        baseDatos.store(new Jefe("Jesús", 3, 5,new Hijo("Noelia", 3)));
        baseDatos.store(new Jefe("Dolores", 5,63,new Hijo("Sergio", 7)));
        baseDatos.store(new Jefe("Vicki", 3, 5,null));
        baseDatos.store(new Jefe("Fátima", 5,63,new Hijo("Lidia", 27)));
        baseDatos.store(new Jefe("Juan Luís", 3, 5,null));
        baseDatos.store(new Jefe("Elena", 1,42,new Hijo("David", 19)));
        baseDatos.store(new Jefe("Miguel", 20,45,new Hijo("Paula", 3)));
        baseDatos.store(new Jefe("Jesús", 19, 44,new Hijo("Rubén", 12)));
        action.cerrarConexion();
    }
    
    public Query conectarBaseDatos(){
        //Abrir la base de datos:
        baseDatos = Db4oEmbedded.openFile("BDJefeHijo.db4o");

        //Creo un objeto Query para realizar la consulta
        Query query = baseDatos.query();
        query.constrain(Jefe.class);
        
        return query;
        
    }
    
    public void cerrarConexion() {
        baseDatos.commit();
        baseDatos.close();
    }

    public void listarJefesMayores() throws UnsupportedEncodingException {
        PrintStream out = new PrintStream(System.out, true, "UTF-8");
        
        //Condiciones de búsqueda:
        Query query = conectarBaseDatos();
        query.descend("edad").constrain(55).greater().or(query.descend("edad").constrain(55).equal());

        //Ejecuto la consulta y guardo el resultado en una ObjectSet de clase Jefe
        ObjectSet resultado = query.execute();
        
        System.out.println("Recuperados " + resultado.size() + " Objetos");
        Jefe jefe;
        while (resultado.hasNext()) {
            jefe = (Jefe)resultado.next();
            out.println();
            out.println("Nombre: " + jefe.getNombre() + "/ antigüedad: " + jefe.getAniosEmpresa() + " años / edad: " + jefe.getEdad() + " años");
        }

        cerrarConexion();
    }
    
    public void sumarEdad(String nombre) throws UnsupportedEncodingException {
        PrintStream out = new PrintStream(System.out, true, "UTF-8");
       
        Query query = conectarBaseDatos();
        
        //Condiciones de búsqueda:
        query.descend("nombre").constrain(nombre).like();

        ObjectSet<Jefe> resultado = query.execute();
        Jefe jefe = resultado.next();
        jefe.setEdad(jefe.getEdad()+1);
        //Guardo el resultado:
        baseDatos.store(jefe);

        out.println("La edad de " + jefe.getNombre() + " ha pasado a " + jefe.getEdad() + " años.");

        cerrarConexion();
    }

    public void borrarJefes() throws UnsupportedEncodingException {
        PrintStream out = new PrintStream(System.out, true, "UTF-8");
       
        Query query = conectarBaseDatos();
        
        //Condiciones de búsqueda:
        query.descend("aniosEmpresa").constrain(6).greater();

        ObjectSet resultado = query.execute();
        System.out.println("Recuperados " + resultado.size() + " Objetos");
        
        
        Jefe jefe;
        out.println("Se procederá a borrar los siguientes jefes:");
        while (resultado.hasNext()) {
            jefe = (Jefe)resultado.next();
            out.println("Nombre: " + jefe.getNombre() + " antigüedad: " + jefe.getAniosEmpresa() + " años: " + jefe.getEdad());
            baseDatos.delete(jefe);
        }
        
        cerrarConexion();
    }
    
    public  void listarJefes() throws UnsupportedEncodingException {
        PrintStream out = new PrintStream(System.out, true, "UTF-8");
        
        //Condiciones de búsqueda:
        Query query = conectarBaseDatos();
        query.constrain(Jefe.class);

        //Ejecuto la consulta y guardo el resultado en una ObjectSet de clase Jefe
        ObjectSet resultado = query.execute();
        
        System.out.println("Recuperados " + resultado.size() + " Objetos");
        Jefe jefe;
        while (resultado.hasNext()) {
            jefe = (Jefe)resultado.next();
            out.println();
            out.print("Nombre: " + jefe.getNombre() + "/ antigüedad: " + jefe.getAniosEmpresa() + " años / edad: " + jefe.getEdad() + " años");
            if (jefe.getHijo()!=null){
                out.print( " // Hijo: " + jefe.getHijo().getNombre() + " edad: " + jefe.getHijo().getEdad() + "años");
            }          
        }

        cerrarConexion();
    }

}