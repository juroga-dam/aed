package JefeHijo;

import java.util.Scanner;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

public class Main{
    
    public static void main(String[] args) throws UnsupportedEncodingException {
        PrintStream out = new PrintStream(System.out, true, "UTF-8");
        Scanner scanner = new Scanner(System.in);
        Actions actions = new Actions();

        //Crear la base de datos
        Actions.crearBD();
        
        while (true) {   
            out.println();
            out.println("Menú:");
            out.println("1. Visualizar los jefes que tengan más de 55 años");
            out.println("2. Modificar la edad de Miguel incrementando su edad un año más");
            out.println("3. Borrar los jefes que llevan más de 6 años en la empresa");
            out.println("4. Visualizar todos los jefes que quedan, incluidos sus hijos");
            out.println("5. Salir");

            out.print("Seleccione una opción mediante su número: ");
            int opcion = scanner.nextInt();
        
            switch (opcion) {
                case 1:
                    out.println("Visualizar los jefes que tengan más de 55 años");
                    actions.listarJefesMayores();
                    break;
                case 2:
                    out.println("Modificar la edad de Miguel incrementando su edad un año más");
                    actions.sumarEdad("Miguel");
                    break;
                case 3:
                    out.println("Borrar los jefes que llevan más de 6 años en la empresa");
                    actions.borrarJefes();
                    break;
                case 4:
                    out.println("Visualizar todos los jefes que quedan, incluidos sus hijos");
                    actions.listarJefes();
                    break;
                case 5:
                    out.println("Saliendo del programa");
                    System.exit(0);
                default:
                    out.println("Opción no válida. Inténtelo de nuevo.");
            }      
        }           
    }
}