package JefeHijo;

/**
 *
 * @author juan
 */
public class Jefe {
    private String nombre;
    private int aniosEmpresa;
    private int edad;
    private Hijo hijo;

    public Jefe(String nombre, int aniosEmpresa, int edad, Hijo hijo) {
        this.nombre = nombre;
        this.aniosEmpresa = aniosEmpresa;
        this.edad = edad;
        this.hijo = hijo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getAniosEmpresa() {
        return aniosEmpresa;
    }

    public void setAniosEmpresa(int aniosEmpresa) {
        this.aniosEmpresa = aniosEmpresa;
    }

    public Hijo getHijo() {
        return hijo;
    }

    public void setHijo(Hijo hijo) {
        this.hijo = hijo;
    }

    @Override
    public String toString() {
        return "Jefe{" + "nombre=" + nombre + ", aniosEmpresa=" + aniosEmpresa + ", edad=" + edad + ", hijo=" + hijo + '}';
    }

    
    
}