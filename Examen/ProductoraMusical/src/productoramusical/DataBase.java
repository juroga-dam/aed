package productoramusical;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
/**
 *
 * @author crass
 */
public class DataBase {
    
    private static DataBase instancia = null;
    private static boolean permitirInstancianueva;
    private String dbFile = "chinook.db"; //Indicar el archivo que contiene la base de datos
    private Connection conexion;
    private Statement smt;
    
    
    DataBase() throws Exception {
        if (!permitirInstancianueva)
            throw new Exception("No se puede crear la instancia, usa getInstance");
    }

    public static DataBase getInstance() {
        if (instancia == null) {
            permitirInstancianueva = true;
            try {
                instancia = new DataBase();
            } catch (Exception e) {
                e.printStackTrace();
            }
            permitirInstancianueva = false;
        }
        return instancia;
    }
    
    public ResultSet consultaBD(String consulta) throws SQLException{
        conexion = DriverManager.getConnection( "jdbc:sqlite:"+dbFile );
        smt = conexion.createStatement();     
        ResultSet rset = smt.executeQuery(consulta);
        return rset;
    }
    
    public void cerrarConsulta() throws SQLException {
        conexion.close();
    }

    public static DataBase getInstancia() {
        return instancia;
    }

    public static void setInstancia(DataBase instancia) {
        DataBase.instancia = instancia;
    }

    public static boolean isPermitirInstancianueva() {
        return permitirInstancianueva;
    }

    public static void setPermitirInstancianueva(boolean permitirInstancianueva) {
        DataBase.permitirInstancianueva = permitirInstancianueva;
    }

    public String getDbFile() {
        return dbFile;
    }

    public void setDbFile(String dbFile) {
        this.dbFile = dbFile;
    }

    public Connection getConexion() {
        return conexion;
    }

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }

    public Statement getSmt() {
        return smt;
    }

    public void setSmt(Statement smt) {
        this.smt = smt;
    }
}

//SELECT * FROM sqlite_master WHERE type = "table";
