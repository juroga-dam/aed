package productoramusical;

/**
 *
 * @author juan
 */
public class Genere {
    private int GenereId;
    private String Name;

    public Genere() {
    }

    public int getGenereId() {
        return GenereId;
    }

    public void setGenereId(int GenereId) {
        this.GenereId = GenereId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }
    
    
}
