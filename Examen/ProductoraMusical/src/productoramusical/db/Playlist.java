package productoramusical.db;

/**
 *
 * @author juan
 */
public class Playlist {
    private int Playlistid;
    private String Name;

    public Playlist(int Playlistid, String Name) {
        this.Playlistid = Playlistid;
        this.Name = Name;
    }

    public int getPlaylistid() {
        return Playlistid;
    }

    public void setPlaylistid(int Playlistid) {
        this.Playlistid = Playlistid;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    @Override
    public String toString() {
        return "Playlist{" + "Playlistid=" + Playlistid + ", Name=" + Name + '}';
    }
    
    
}
