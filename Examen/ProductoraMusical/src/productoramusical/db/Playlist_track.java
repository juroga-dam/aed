/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package productoramusical.db;

/**
 *
 * @author juan
 */
public class Playlist_track {
    
    private int PlaylistId;
    private int TrackId;

    public Playlist_track(int PlaylistId, int TrackId) {
        this.PlaylistId = PlaylistId;
        this.TrackId = TrackId;
    }

    public int getPlaylistId() {
        return PlaylistId;
    }

    public void setPlaylistId(int PlaylistId) {
        this.PlaylistId = PlaylistId;
    }

    public int getTrackId() {
        return TrackId;
    }

    public void setTrackId(int TrackId) {
        this.TrackId = TrackId;
    }

    @Override
    public String toString() {
        return "Playlist_track{" + "PlaylistId=" + PlaylistId + ", TrackId=" + TrackId + '}';
    }
    
    
    
}
