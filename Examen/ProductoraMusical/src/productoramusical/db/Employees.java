package productoramusical.db;

import java.util.Date;

/**
 *
 * @author juan
 */
public class Employees {
    private int EmployeeId;
    private String LastName;
    private String FirstName;
    private String Title;
    private int ReportsTo;
    private Date BithDate;
    private Date HireDate;
    private String Address;

    public Employees(int EmployeeId, String LastName, String FirstName, String Title, int ReportsTo, Date BithDate, Date HireDate, String Address) {
        this.EmployeeId = EmployeeId;
        this.LastName = LastName;
        this.FirstName = FirstName;
        this.Title = Title;
        this.ReportsTo = ReportsTo;
        this.BithDate = BithDate;
        this.HireDate = HireDate;
        this.Address = Address;
    }

    public int getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(int EmployeeId) {
        this.EmployeeId = EmployeeId;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public int getReportsTo() {
        return ReportsTo;
    }

    public void setReportsTo(int ReportsTo) {
        this.ReportsTo = ReportsTo;
    }

    public Date getBithDate() {
        return BithDate;
    }

    public void setBithDate(Date BithDate) {
        this.BithDate = BithDate;
    }

    public Date getHireDate() {
        return HireDate;
    }

    public void setHireDate(Date HireDate) {
        this.HireDate = HireDate;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    @Override
    public String toString() {
        return "Employees{" + "EmployeeId=" + EmployeeId + ", LastName=" + LastName + ", FirstName=" + FirstName + ", Title=" + Title + ", ReportsTo=" + ReportsTo + ", BithDate=" + BithDate + ", HireDate=" + HireDate + ", Address=" + Address + '}';
    }
    
    
}
