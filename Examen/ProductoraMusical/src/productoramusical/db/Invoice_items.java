package productoramusical.db;

/**
 *
 * @author juan
 */
public class Invoice_items {
    private int InvoiceItemId;
    private int InvoiceId;
    private int TrackId;
    private float Unitprice;
    private int Quantity;

    public Invoice_items(int InvoiceItemId, int InvoiceId, int TrackId, float Unitprice, int Quantity) {
        this.InvoiceItemId = InvoiceItemId;
        this.InvoiceId = InvoiceId;
        this.TrackId = TrackId;
        this.Unitprice = Unitprice;
        this.Quantity = Quantity;
    }

    public int getInvoiceItemId() {
        return InvoiceItemId;
    }

    public void setInvoiceItemId(int InvoiceItemId) {
        this.InvoiceItemId = InvoiceItemId;
    }

    public int getInvoiceId() {
        return InvoiceId;
    }

    public void setInvoiceId(int InvoiceId) {
        this.InvoiceId = InvoiceId;
    }

    public int getTrackId() {
        return TrackId;
    }

    public void setTrackId(int TrackId) {
        this.TrackId = TrackId;
    }

    public float getUnitprice() {
        return Unitprice;
    }

    public void setUnitprice(float Unitprice) {
        this.Unitprice = Unitprice;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int Quantity) {
        this.Quantity = Quantity;
    }

    @Override
    public String toString() {
        return "Invoice_items{" + "InvoiceItemId=" + InvoiceItemId + ", InvoiceId=" + InvoiceId + ", TrackId=" + TrackId + ", Unitprice=" + Unitprice + ", Quantity=" + Quantity + '}';
    }
    
    
}
