package productoramusical.db;

/**
 *
 * @author juan
 */
public class Albums {
    private int AlbumId;
    private String Title;
    private int ArtistId;

    public Albums(int AlbumId, String Title, int ArtistId) {
        this.AlbumId = AlbumId;
        this.Title = Title;
        this.ArtistId = ArtistId;
    }

    public int getAlbumId() {
        return AlbumId;
    }

    public void setAlbumId(int AlbumId) {
        this.AlbumId = AlbumId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public int getArtistId() {
        return ArtistId;
    }

    public void setArtistId(int ArtistId) {
        this.ArtistId = ArtistId;
    }

    @Override
    public String toString() {
        return "Albums{" + "AlbumId=" + AlbumId + ", Title=" + Title + ", ArtistId=" + ArtistId + '}';
    }
    
    
}
