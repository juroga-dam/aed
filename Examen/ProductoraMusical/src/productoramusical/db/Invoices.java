package productoramusical.db;

import java.util.Date;

/**
 *
 * @author juan
 */
public class Invoices {
    private int InvoiceId;
    private int CustomerId;
    private Date InvoiceDate;
    private String BillingAddress;
    private String BillingCity;

    public Invoices(int InvoiceId, int CustomerId, Date InvoiceDate, String BillingAddress, String BillingCity) {
        this.InvoiceId = InvoiceId;
        this.CustomerId = CustomerId;
        this.InvoiceDate = InvoiceDate;
        this.BillingAddress = BillingAddress;
        this.BillingCity = BillingCity;
    }

    public int getInvoiceId() {
        return InvoiceId;
    }

    public void setInvoiceId(int InvoiceId) {
        this.InvoiceId = InvoiceId;
    }

    public int getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(int CustomerId) {
        this.CustomerId = CustomerId;
    }

    public Date getInvoiceDate() {
        return InvoiceDate;
    }

    public void setInvoiceDate(Date InvoiceDate) {
        this.InvoiceDate = InvoiceDate;
    }

    public String getBillingAddress() {
        return BillingAddress;
    }

    public void setBillingAddress(String BillingAddress) {
        this.BillingAddress = BillingAddress;
    }

    public String getBillingCity() {
        return BillingCity;
    }

    public void setBillingCity(String BillingCity) {
        this.BillingCity = BillingCity;
    }

    @Override
    public String toString() {
        return "Invoices{" + "InvoiceId=" + InvoiceId + ", CustomerId=" + CustomerId + ", InvoiceDate=" + InvoiceDate + ", BillingAddress=" + BillingAddress + ", BillingCity=" + BillingCity + '}';
    }
    
    
}
