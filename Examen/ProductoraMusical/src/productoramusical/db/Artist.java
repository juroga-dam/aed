package productoramusical.db;

/**
 *
 * @author juan
 */
public class Artist {
    
    private int ArtistId;
    private String Name;

    public Artist(int ArtistId, String Name) {
        this.ArtistId = ArtistId;
        this.Name = Name;
    }

    public int getArtistId() {
        return ArtistId;
    }

    public void setArtistId(int ArtistId) {
        this.ArtistId = ArtistId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    @Override
    public String toString() {
        return "Artist{" + "ArtistId=" + ArtistId + ", Name=" + Name + '}';
    }
    
    
}
