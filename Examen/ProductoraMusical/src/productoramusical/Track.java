package productoramusical;

/**
 *
 * @author juan
 */
public class Track {
    
    private int TrackId;
    private String Name;
    private int AlbumId;
    private int MediaTypeId;
    private int GenereId;
    private String Composer;
    private int Milliseconds;
    private int Bytes;
    private float UnitPrice;

    public Track(int TrackId, String Name, int AlbumId, int MediaTypeId, int GenereId, String Composer, int Milliseconds, int Bytes, float UnitPrice) {
        this.TrackId = TrackId;
        this.Name = Name;
        this.AlbumId = AlbumId;
        this.MediaTypeId = MediaTypeId;
        this.GenereId = GenereId;
        this.Composer = Composer;
        this.Milliseconds = Milliseconds;
        this.Bytes = Bytes;
        this.UnitPrice = UnitPrice;
    }

    public Track() {
    }
    
    

    public int getTrackId() {
        return TrackId;
    }

    public void setTrackId(int TrackId) {
        this.TrackId = TrackId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getAlbumId() {
        return AlbumId;
    }

    public void setAlbumId(int AlbumId) {
        this.AlbumId = AlbumId;
    }

    public int getMediaTypeId() {
        return MediaTypeId;
    }

    public void setMediaTypeId(int MediaTypeId) {
        this.MediaTypeId = MediaTypeId;
    }

    public int getGenereId() {
        return GenereId;
    }

    public void setGenereId(int GenereId) {
        this.GenereId = GenereId;
    }

    public String getComposer() {
        return Composer;
    }

    public void setComposer(String Composer) {
        this.Composer = Composer;
    }

    public int getMilliseconds() {
        return Milliseconds;
    }

    public void setMilliseconds(int Milliseconds) {
        this.Milliseconds = Milliseconds;
    }

    public int getBytes() {
        return Bytes;
    }

    public void setBytes(int Bytes) {
        this.Bytes = Bytes;
    }

    public float getUnitPrice() {
        return UnitPrice;
    }

    public void setUnitPrice(float UnitPrice) {
        this.UnitPrice = UnitPrice;
    }

    @Override
    public String toString() {
        return "Tracks{" + "TrackId=" + TrackId + ", Name=" + Name + ", AlbumId=" + AlbumId + ", MediaTypeId=" + MediaTypeId + ", GenereId=" + GenereId + ", Composer=" + Composer + ", Milliseconds=" + Milliseconds + ", Bytes=" + Bytes + ", UnitPrice=" + UnitPrice + '}';
    }
    
    
}
