package productoramusical;

import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
/**
 *
 * @author juan
 */
public class ProductoraMusical {

   //SELECT * FROM sqlite_master WHERE type = "table";
        
    private static void listDataBase() {
               
	try {
            DataBase instance = DataBase.getInstance();
            ResultSet resultSet = instance.consultaBD("SELECT * FROM sqlite_master WHERE type = \"table\"");
            var rsmd = resultSet.getMetaData();
            int columnsNumber = rsmd.getColumnCount();
            while (resultSet.next()) {
                for (int i = 1; i <= columnsNumber; i++) {
                    if (i > 1) System.out.print(",  ");
                    String columnValue = resultSet.getString(i);
                    System.out.print(columnValue + " " + rsmd.getColumnName(i));
                }
                System.out.println("");
            }
            instance.cerrarConsulta();
        } catch (SQLException e) {
            e.printStackTrace();
	}
    }
    
    
    private static List<Customer> listCustomers() {

	List<Customer> customers = new ArrayList<>();
	try {
            DataBase instance = DataBase.getInstance();
            ResultSet resultSet = instance.consultaBD("select * from customers");
            
            while (resultSet.next()) {
		Customer customer = new Customer();
		customer.setCustomerId(resultSet.getInt("CustomerId"));
		customer.setFirstName(resultSet.getString("FirstName"));
		customer.setLastName(resultSet.getString("LastName"));
                customer.setCompany(resultSet.getString("Company"));
                customer.setAddress(resultSet.getString("Address"));
                customer.setCity(resultSet.getString("City"));
                customer.setState(resultSet.getString("State"));
                customer.setCountry(resultSet.getString("Country"));
                customer.setPostalCode(resultSet.getString("PostalCode"));
                customer.setPhone(resultSet.getString("Phone"));
                customer.setFax(resultSet.getString("Fax"));
                customer.setEmail(resultSet.getString("Email"));
                customer.setSupportRepId(resultSet.getInt("SupportRepId"));
		customers.add(customer);
            }
            instance.cerrarConsulta();
        } catch (SQLException e) {
            e.printStackTrace();
	}
            return customers;
    }
    
    private static List<Album> listAlbums(String artist) {

	List<Album> albums = new ArrayList<>();
	try {
            DataBase instance = DataBase.getInstance();
            ResultSet resultSet = instance.consultaBD("SELECT AlbumId, Title FROM albums WHERE ArtistId IN (SELECT ArtistId FROM artists WHERE name = '"+artist+"');");
            
            while (resultSet.next()) {
		Album album = new Album();
                album.setAlbumId(resultSet.getInt("AlbumId"));
                album.setTitle(resultSet.getString("Title"));
		albums.add(album);
            }
            instance.cerrarConsulta();
        } catch (SQLException e) {
            e.printStackTrace();
	}
            return albums;
    }
    
    private static List<MediaType> listMediaType() {

	List<MediaType> mediaTypes = new ArrayList<>();
	try {
            DataBase instance = DataBase.getInstance();
            ResultSet resultSet = instance.consultaBD("SELECT * FROM media_types");
            
            while (resultSet.next()) {
		MediaType mediaType = new MediaType();
                mediaType.setMediaTypeId(resultSet.getInt("MediaTypeId"));
                mediaType.setName(resultSet.getString("Name"));
		mediaTypes.add(mediaType);
            }
            instance.cerrarConsulta();
        } catch (SQLException e) {
            e.printStackTrace();
	}
            return mediaTypes;
    }
    

    
    /*private static void InsertarCancion(String Name, String Composer, int TrackId, int AlbumId, int MediaTypeId, int GenereId, int Milliseconds, int Bytes,  float UnitPrice) throws UnsupportedEncodingException{
        PrintStream out = new PrintStream(System.out, true, "UTF-8");
        
        Track track = new Track();

        track.setTrackId(TrackId);
        track.setName(Name);
        track.setAlbumId(AlbumId);
        track.setMediaTypeId(MediaTypeId);
        track.setGenereId(GenereId);
        track.setComposer(Composer);
        track.setMilliseconds(Milliseconds);
        track.setBytes(Bytes);
        track.setUnitPrice(UnitPrice);
        
        try {
            DataBase instance = DataBase.getInstance();
            instance.consultaBD(String.format("insert into tracks values ('%s','%s','%s','%s','%d','%d','%d','%d')",
											track.getTrackId();
                                                                                        track.getName();
                                                                                        track.getAlbumId();
                                                                                        track.getMediaTypeId();
                                                                                        track.getGenereId();
                                                                                        track.getComposer();
                                                                                        track.getMilliseconds();
                                                                                        track.getBytes();
                                                                                        track.gettUnitPrice()));
            instance.cerrarConsulta();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Ha ocurrido un error al introducir el el track");
	}
        
    }*/
    
    
    public static void main(String[] args) throws UnsupportedEncodingException {
        PrintStream out = new PrintStream(System.out, true, "UTF-8");
        Scanner scanner = new Scanner(System.in);
        
        while (true) {   
            out.println();
            out.println("Menú:");
            out.println("1. Mostrar y pedir información general de la Base de Datos");
            out.println("2. Mostrar información de la Tabla \"Customers\"");
            out.println("3. Ver la información de los álbumes de un artista específico");
            out.println("4. Insertar una nueva canción en la tabla \"Tracks\"");
            out.println("5. Borrar un album por su ID");
            out.println("6. Modificar el género de una canción");
            out.println("7. Salir");

            out.print("Seleccione una opción mediante su número: ");
            int opcion = scanner.nextInt();
        
            switch (opcion) {
                case 1:
                    out.println("Mostrar y pedir información general de la Base de Datos");
                    listDataBase();                            
                    break;
                case 2:
                    out.println("Mostrar información de la Tabla \"Customers\"");
                    List<Customer> customers = listCustomers();
                    
                    for (Customer customer : customers) {
                    out.println(String.format("----------------------\nID: %d \nNombre: %s \nApellido: %s \nCompañia: %s\nDirección: %s \nCiudad: %s \nEstado: %s \nPais: %s \nCódigo Postal: %s \nTeléfono: %s \nFax: %s \nEmail: %s \nSoporte: %d \n", 
                                                                                                        customer.getCustomerId(),
                                                                                                        customer.getFirstName(),
                                                                                                        customer.getLastName(),
                                                                                                        customer.getCompany(),
                                                                                                        customer.getAddress(),
                                                                                                        customer.getCity(),
                                                                                                        customer.getState(),
                                                                                                        customer.getCountry(),
                                                                                                        customer.getPostalCode(),
                                                                                                        customer.getPhone(),
                                                                                                        customer.getFax(),
                                                                                                        customer.getEmail(),
                                                                                                        customer.getSupportRepId()));
                    }
                    break;
                case 3:
                    out.println("Ver la información de los álbumes de un artista específico");
                    out.print("Introduzca el nombre del artista a buscar: ");
                    String artist = scanner.next();
                    List<Album> albums = listAlbums(artist);
 
                    for (Album album : albums) {
                    out.println(String.format("----------------------\nId Album: %d \nTítulo: %s", 
                                                                                album.getAlbumId(),
                                                                                album.getTitle()));
                    }
                    break;
                case 4:
                    out.println("Insertar una nueva canción en la tabla \"Tracks\"");
                    out.println("\nIntroduzca los datos de la nueva canción:");
                    String Name, Composer;
                    int TrackId, AlbumId, MediaTypeId, GenereId, Milliseconds, Bytes;
                    float UnitPrice;
                    //Scanner scanner = new Scanner(System.in);

                    out.print("Introduzca nombre del artista: ");
                    artist = scanner.next();
                    albums = listAlbums(artist);
                    
                    out.println("Los siguientes albums pertenecen a "+artist+":");
                    for (Album album : albums) {
                    out.println(String.format("----------------------\nId Album: %d \nTítulo: %s", 
                                                                                album.getAlbumId(),
                                                                                album.getTitle()));
                    }
                    
                    out.print("\nIndique el número del album sobre el que va a añadir la canción:");
                    AlbumId = scanner.nextInt();

                    List<MediaType> mediaTypes = listMediaType();
 
                    for (MediaType mediaType : mediaTypes) {
                    out.println(String.format("----------------------\nId media: %d \nNombre: %s",mediaType.getMediaTypeId(), mediaType.getName()));
                    }
                    
                    out.print("\nIntroduzca número identificador del tipo de media: ");
                    MediaTypeId = scanner.nextInt();

                    out.print("Introduzca número identificador de género: ");
                    GenereId = scanner.nextInt();

                    out.print("Introduzca nombre del compositor: ");
                    Composer = scanner.next();

                    out.print("Introduzca el tiempo en milisegundos: ");
                    Milliseconds = scanner.nextInt();

                    out.print("Introduzca los bytes que ocupa la canción: ");
                    Bytes = scanner.nextInt();
                    
                    out.print("Introduzca el precio unitario de la canción: ");
                    UnitPrice = scanner.nextFloat();
                    
                    out.println("");
                    break;
                case 5:
                    out.println("Borrar un album por su ID");
                    
                    break;
                case 6:
                    out.println("Modificar el género de una canción");
                    
                    break;
                case 7:
                    out.println("Saliendo del programa");
                    System.exit(0);
                default:
                    out.println("Opción no válida. Inténtelo de nuevo.");
            }      
        } 
    }
    
}
